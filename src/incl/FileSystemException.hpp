#ifndef FILEMANAGER_FILESYSTEMEXCEPTION_HPP
#define FILEMANAGER_FILESYSTEMEXCEPTION_HPP

#include <iostream>
/*! \brief File System Excpetion
* This class represents an exception that is thrown by low-evel side of application.
* It is used to prevent faults of an application and understand which part of system caused a mistake.
*/
class FileSystemException: public std::exception{
public:
    FileSystemException(const std::string &m): std::exception(), message(m){}
     
    virtual const char* what() const noexcept override{
        return message.data();
    }
private:
    std::string message; ///Message of an exception
};

#endif //FILEMANAGER_FILESYSTEMEXCEPTION_HPP