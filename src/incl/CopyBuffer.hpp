#ifndef FILEMANAGER_COPYBUFFER_HPP
#define FILEMANAGER_COPYBUFFER_HPP

#include <iostream>
#include <vector>
#include "./FileAbstract.hpp"
#include <queue>
namespace buffer{
    /*! \brief Buffer for Copy/Paste operation
    *
    * This is a class that is used in copying and pasting operations.
    */
    class CopyBuffer{
        public:
        CopyBuffer() = default;

        /*! \brief Erasing the list
        *
        * This method erases the list of files in the buffer
        */
        void Erase(){
            std::queue<std::shared_ptr<FileAbstract> > empty;
            buffer.swap(empty);
        }
        
        /*! \brief Getter of the queue  
        *
        * This method returns a list of all files in the buffer formed in queue container
        * \return Contents of buffer
        */
        std::queue<std::shared_ptr<FileAbstract> >& GetQueue(){
            return buffer;
        }

        /*! \brief Getter of origin
        * \return Absolute path to origin
        */
        std::string GetOrigin(){
            return origin;
        }

        /*! \brief Pudhing value to queue
        *\param val Value to be pushed
        */
        void Push(const std::shared_ptr<FileAbstract> &val){
            buffer.push(val);
        }

        /*! \brief Setting of origin
        *\param val Nre absolute path to origin.
        */
        void setOrigin(const std::string &val){
            origin = val;
        }

        private:
        std::queue<std::shared_ptr<FileAbstract> > buffer; /// List of files in the buffer
        std::string origin; /// Absolute path to the origin of the buffer
    };
}
#endif //FILEMANAGER_COPYBUFFER_HPP
