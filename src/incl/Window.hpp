#ifndef FILEMANAGER_WINDOW_HPP
#define FILEMANAGER_WINDOW_HPP

#include <iostream>
#include <vector>
#include <ncurses.h>
#include "FileAbstract.hpp"
#include "Folder.hpp"
#include "CopyBuffer.hpp"
#include "File.hpp"
#include "Constants.hpp"
#include "Link.hpp"

/*!
 * \brief UI class.
 *
 * This class represents all UI-related functions. It handles window, drawing, opening direcories, creating/deleting
 * and copying/pasting of folder/file.
 */
class Window {
public:
    Window(): selectedIndex(0){
        Init(); 
        Folder folder =Folder("/");
        filesList =folder.Scan();
        for(auto i: filesList){
            markedFiles.push_back(false);
        }
    }

    /*! \brief NCurses Initialization method.
     *
     * This method initializes basic functions of NCurses that will be used further. It sets keyboard and colours pair.
     */
    void Init();

    /*! \brief Method that erases current screen.
     *
     * This method erases all contents of current window.
     */
    void Erase();

    /*! \brief Method that closes current window
     */
    void Close();

    /*! \brief Method for drawing the application window.
     *
     * This method draws UI of application. It draws window and represents list of
     * current folder files/directories. By default it is a home (~) directoy.
     */
    void Draw(const long lowerIndex = 0, const size_t upperIndex = MAX_LINES-1);

    /*! \brief Method that opens some specific folder.
     *
     *  This method tries to open folder given as a parameter. It checks permissions and,
     *  if this operation is allowed, it opens this directiry. Otherwise, it notifies
     *  user that something went wrong.
     *  \param dir Folder object that should be opened
     */
    void OpenFolder();

    /*! \brief Method that searches all files according to some regular expression.
     *
     * This method searches for all files whose name satisfies given expression.
     * This expression can contain * symbol, which means any symbol.
     * \param expr Regular expression that filters files from current directory.
     * \return List of folders/files whose names are satisfying regular expression.
     */
    std::vector<std::shared_ptr<FileAbstract> > Search(const std::string &expr);


    /*! \brief Method that copies selected file/folder.
     *
     * This method recursively copies selected folder/file into the buffer which is one of the class fields. If
     * permission to the file is denied then this file is skipped.
     * \param file File/Folder to copy.
     */
    void Copy();

    /*! \brief Method that pastes contents of buffer.
     *
     * This method pastes current contents of buffer (if it's not empty) into selected place.
     * If file is currently selected, the buffer is pasted in the current folder. Otherwise
     * (if folder is being selected), the buffer is pasted into the selected folder.
     */
    void Paste();

    /*! \brief Getter of selected index of file in current directory file list
     * \return Current selected index in the list.
     */
    const long &Index() const{
        return selectedIndex;
    }

    /*! \brief Method that increments index of selected element in list of files in directory
     */
    void IncrementIndex(){
        selectedIndex++;
    }

    /*! \brief Method that decrements index of selected element in list of files in directory
     */
    void DecrementIndex(){
        selectedIndex--;
    }

    /*!
     * \brief Setter of selected index of file in current directory file list
     * /param val New value of selected index.
     */
    void SetIndex(const long& val){
        selectedIndex = val;
    }

    /*! \brief Getter of current directory file list
     * \return List of files/directories
     */
    std::vector<std::shared_ptr<FileAbstract> > &List(){
        return filesList;
    }

    /*! \brief Setter of current directory file list
     * \param New value
     */
    void SetList(const std::vector<std::shared_ptr<FileAbstract> > &val){
        filesList = val;
    }

    /*! \brief Function that draws new name dialog
     * 
     * This method draws a new window that is used for getting a name of a created file.
     * 
     * \param win Created window
     * \param helloText Heading of the window
     * \param curName Current name
     */
    void DrawNameDialog(WINDOW *win, const std::string &helloText, const std::string &curName);
    
    /*! \brief Function that gets name of new file using GUI
     * 
     * This function is used for getting a name of the file from GUI. It generates
     * new window and handles keyboard events
     * 
     * \param fileType Type of file that should be written in the heading
     * \return Name of the file
     */
    std::string GetName(const std::string &fileType);
    
    /*! \brief Function that gets regular expression from GUI
     * 
     * This function is used for getting a regular expression from GUI. It generates
     * new window and handles keyboard events
     * 
     * \return Regular expression
     */
    std::string GetRegEx();

    /*! \brief Function that adds file to the list
     * 
     * \param file File that is to be pushed
     */
    void AddFile(const File &file){
        filesList.push_back(std::make_shared<File>(file));
    }

    /*! \brief Function that adds folder to the list
     * 
     * \param folder Folder that is to be pushed
     */
    void AddFolder(const Folder &folder){
        filesList.push_back(std::make_shared<Folder>(folder));
    }

    /*! \brief Function that adds link to the list
     * 
     * \param link Link that is to be pushed
     */
    void AddLink(const Link &link){
        filesList.push_back(std::make_shared<Link>(link));
    }

    /*! \brief Function that removes file from the list
     * 
     * \param file File that is to be pushed
     */
    void DeleteFromVector(std::shared_ptr<FileAbstract> file){
        filesList.erase(std::remove(filesList.begin(), filesList.end(), file), filesList.end());
    }

    /*! \brief Function adds file to list of selected files in multiple selection mode.
     * 
     */
    void MultipleMode_Add(){
        selectedFiles.push_back(filesList[selectedIndex]);
    }

    /*! \brief Getter of selected files in multiple selection mode
     * 
     * \return List of files
     */
    std::vector<std::shared_ptr<FileAbstract> > Files(){
        return selectedFiles;
    }

    /*! \brief Function that generates GUI with error message
     * 
     * \param message Error description that is to be displayed
     */
    void ShowErrorDiaglog(const std::string message);

    /*! \brief Function that marks file as selected in multiple selection mode
     * 
     * \param lowerIndex Lower index in the list that is to be displayed
     * \param upperIndex Upper index in the list that is to be displayed
     */
    void MarkSelected(long lowerIndex, long upperIndex){
        markedFiles[selectedIndex] = !markedFiles[selectedIndex];
        Erase();
        Draw(lowerIndex, upperIndex);
    }

    /*! \brief Function that resets list of selected files
     * 
     * \param lowerIndex Lower index in the list that is to be displayed
     * \param upperIndex Upper index in the list that is to be displayed
     */
    void ResetSelected(long lowerIndex, long upperIndex){
        for(auto i = markedFiles.begin(); i!= markedFiles.end(); ++i){
            *i = false;
        }
        Erase();
        Draw(lowerIndex, upperIndex);
    }

    /*! \brief Getter of the buffer
     * 
     * \return Copy buffer
     */
    buffer::CopyBuffer* Buffer(){
        return &buffer;
    }

private:
    std::vector<std::shared_ptr<FileAbstract> > filesList; ///list of files/folders in current directory
    long selectedIndex; ///current selected index in directory
    buffer::CopyBuffer buffer; ///copying buffer (can be empty)
    std::vector<std::shared_ptr<FileAbstract> > selectedFiles; ///list of selected files in multiple selection mode
    std::vector<bool> markedFiles; /// list of marked file in multiple selecting mode.
};


#endif //FILEMANAGER_WINDOW_HPP
