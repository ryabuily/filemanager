#ifndef FILEMANAGER_FILEABSTRACT_HPP
#define FILEMANAGER_FILEABSTRACT_HPP

#include <iostream>
#include <sys/stat.h>
#include <sys/types.h>
#include <libgen.h>

/*! \brief Abstract entity class.
 *
 * This is the basic class that is responsible for work with files/directories. It provides basic functionality
 * that should be implemented in descendants.
 */
namespace buffer{
    class CopyBuffer;
}


class FileAbstract{
public:
    ///Types of file
    enum FileType{
        /// Type is not selected (default)
        NONE,
        /// Regular file
        FILE,
        ///Symbolic link
        LINK,
        ///Directory
        DIRECTORY
    };
    explicit FileAbstract(const std::string &n = "/"): absolutePath(n){}
    FileAbstract(const FileAbstract &other) = default;
    virtual ~FileAbstract() = default;
    /*! \brief Method checking the directiry status.
     *
     * This method says whether file with given path is a directory or not.
     * \param path Path of the file
     * \return positive number if path is a directory, 0 if not and -1 if error occured
     */
    static int isDirectory(const char *path);
    
    /*! \brief Current directory name
     *
     * This method returns directory name of current path. It parses string from the end until first '/'
     * symbol. Then it return a substring from beginning until this symbol.
     * \param current Current path
     * \return Directiry name
     */
    static std::string DirName(const std::string &current);

    /*! \brief Method for creating an entity.
     *
     * This method creates an entity and provides it with basic permission (full for admin and reading & writing
     * for current user and user groups).
     * \return 0 if file created and -1 if there was an error
     */
    virtual int Create()=0;

    /*! \brief Method for deleting an entity.
     *
     * This method deletes an entity. If file couldn't be deleted (file doesn't exist, permission denied) then
     * method will generate an exception.
     * \return 0 if file deleted and -1 if there was an error
     */
    virtual int Delete() = 0;

    /*! \brief Method for entity copying.
     *
     * This method is used for copying of the entity into the buffer. If some file from the directory couldn't be
     * reached, it'll be skipped.
     * 
     * \param buffer Special buffer that is used to keep file copied.
     * \param isClear Flag that sets buffer erasing
     */
    virtual void Copy( buffer::CopyBuffer *buffer, bool isClear) = 0;

    /*! \brief Method for entity pasting.
     *
     * This method is used for pasting of the entity from the buffer. It will paste the content of buffer in
     * the currently opened directory.
     * 
     * \param buffer Special buffer which contents will be pasted from.
     */
    virtual void Paste(const std::string &destination, buffer::CopyBuffer *buffer) =0;

    /*! \brief Method for entity renaming.
     *
     * This method is used for renaming the entity. It will change its name in UI and update absolute path
     * to the entity.
     * 
     * \param newName New name of file
     * \return -1 is there was some mistake and something positive if it was successfully renamed.
     */
    int Rename(const std::string &newName);

    /*! \brief Getter of absolute path to the entity
    * \return Current absolute path
     */
    std::string Path() const {
        return absolutePath;
    }
    /*! \brief Setter of absolute path to the entity
    *
    * \param val New value of absolute path
     */
    void SetPath(const std::string &val){
        absolutePath = val;
    }

    /*! \brief Getter of name of current file
    * \return True if navigation was successful and false of not.
     */
    std::string Name() const{
        return basename(const_cast<char*>(absolutePath.data()));
    }

    /*! \brief Setter of directory flag
    *
    * This flag is set to -1 by default (file hasn't been preccessed yet). Value 0
    * means that it's a file. Otherwise, it's a link.
    *
    * \param val New value of directory flag
     */
    void SetDir(const FileType val){
        dir = val;
    }


    /*! \brief Navigating method
    *
    * This method performs a navigation to the selected folder. 
    * 
    * \param targetContent Content of current folder
    * \return True if navigation was successful and false of not.
     */
    FileType Dir(){
        return dir;
    }
    /*! \brief Navigating method
    *
    * This method performs a navigation to the selected folder. 
    * 
    * \param targetContent Content of current folder
    * \return True if navigation was successful and false of not.
     */
    virtual bool Navigate(std::vector<std::shared_ptr<FileAbstract> >& targetContent);

    /*! \brief Setter of name of current file
    * \return True if file is directory and false if not (either file or link).
    */
    virtual bool isDir();


protected:
    std::string absolutePath; ///absolute path to the entity
    mode_t permission = 0744; /// default permission of the file
    FileType dir = NONE; /// flag that tells whether current file is a directory
};

#endif //FILEMANAGER_FILEABSTRACT_HPP
