#ifndef FILEMANAGER_FOLDER_HPP
#define FILEMANAGER_FOLDER_HPP

#include <iostream>
#include <vector>
#include "FileAbstract.hpp"
#include "FileContainer.hpp"
#include "CopyBuffer.hpp"


/*! \brief Folder class.
 *
 * This class is one of descendants from FileAbstract class. It's used for implementing basic operations for folders:
 * creating/deleting, renaming, scanning the contents and copying/pasting.
 */
class Folder: public FileContainer{
public:
    explicit Folder(const std::string &name):FileContainer(name){dir = FileAbstract::FileType::DIRECTORY;}
    int Create() override;
    int Delete() override;

    void Copy( buffer::CopyBuffer *buffer, bool isClear) override;
    void Paste(const std::string &destination, buffer::CopyBuffer *buffer) override;

    std::vector<std::shared_ptr<FileAbstract> > Scan() override;
    std::vector<std::shared_ptr<FileAbstract> > CollectAll() override;
};

#endif //FILEMANAGER_FOLDER_HPP
