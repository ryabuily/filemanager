#ifndef FILEMANAGER_FILE_HPP
#define FILEMANAGER_FILE_HPP

#include "FileAbstract.hpp"
#include <iostream>

/*! \brief File class.
 *
 * This class is one of descendants from FileAbstract class. It's used for implementing basic operations for files:
 * creating/deleting, renaming, scanning the contents and copying/pasting.
 */
class File: public FileAbstract{
public:
    explicit File(const std::string &name): FileAbstract(name){dir = FileAbstract::FileType::FILE;}
    File(const File &other)= default;

    int Create() override;
    int Delete() override;

    void Copy( buffer::CopyBuffer *buffer, bool isClear) override;
    void Paste(const std::string &newName, buffer::CopyBuffer *buffer) override;

};

#endif //FILEMANAGER_FILE_HPP
