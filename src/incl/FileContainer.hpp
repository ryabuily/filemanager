#ifndef FILEMANAGER_FILECONTAINER_HPP
#define FILEMANAGER_FILECONTAINER_HPP

#include <vector>
#include "FileAbstract.hpp"

/*! \brief Class represeting container of files
 *
 * This class is responsible for directiry-specific functionality: scanning the folder and collecting
 * all the values. Also, there's a navigation option. The reason to create such a class is need to have
 * directory features being supported to symbolic link as well.
 */
class FileContainer: public FileAbstract{
public:
    explicit FileContainer(const std::string &n): FileAbstract(n){}
    
    /*! \brief Scanning method
    *
    * This method scans the content of the directory. If it containts another directory
    * then it doesn't go into that nested folder. 
    * 
    * /return Contents of current directory
    */
    virtual std::vector<std::shared_ptr<FileAbstract> > Scan()=0;

    /*! \brief Collecting method
    *
    * This method collecting the content of the directory. If it containts another directory
    * then it goes recursively and collects content of this directory. 
    * 
    * /return Contents of current directory
    */
    virtual std::vector<std::shared_ptr<FileAbstract> > CollectAll()=0;

    virtual bool Navigate(std::vector<std::shared_ptr<FileAbstract> >& targetContent) override;

protected:
    std::vector<std::shared_ptr<FileAbstract> > filesList; /// List of files in current directory
};

#endif //FILEMANAGER_FILECONTAINER_HPP