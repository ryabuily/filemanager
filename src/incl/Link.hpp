#ifndef FILEMANAGER_LINK_HPP
#define FILEMANAGER_LINK_HPP

#include "File.hpp"
#include "FileAbstract.hpp"
#include"Folder.hpp"
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>

/*! \brief Class representing symbolic link.
 *
 * This class respresents symbolic link. It can create a link from its path and define origin,
 * 
 */
class Link: public File{
public:
    Link(const std::string &n);

    explicit Link(const std::string &r, const std::string &n): File(r), ref(r), name(n),target(nullptr){
        dir = FileType::LINK;
        if(FileAbstract::isDirectory(ref.data()))
            target.reset(new Folder(ref));
        else target.reset(new File(ref));
    }

    int Create() override;
    bool Navigate(std::vector<std::shared_ptr<FileAbstract> > &targetContent) override {
        return target->Navigate(targetContent);
    }
    
    bool isDir() override {
        return target->isDir();
    }

protected:
    std::string ref; /// Source to the origin
    std::string name; /// Source to the link itself 
    std::shared_ptr<FileAbstract> target; /// content of origin directory (if it's link to directory)
};

#endif //FILEMANAGER_LINK_HPP