#ifndef FILEMANAGER_LOGIC_HPP
#define FILEMANAGER_LOGIC_HPP

#include <iostream>
#include <vector>
#include "FileAbstract.hpp"
#include "Window.hpp"
#include "../incl/Constants.hpp"

/*! \brief Logic Class
* This class represents all events produced by the application. The inspiration for that was from 
the Event Bus design pattern.
*/
class Logic {
public:
    /*! \brief Handler of scrolling down the list
    * \param win Current window
    * \param lowerIndex Lower index that is shown in the list
    * \param upperIndex Upper index that is shown in the list
    */
    static void ButtonPressed_Down (Window &win, long &lowerIndex, long &upperIndex);
    
    /*! \brief Handler of scrolling up the list
    * \param win Current window
    * \param lowerIndex Lower index that is shown in the list
    * \param upperIndex Upper index that is shown in the list
    */
    static void ButtonPressed_Up (Window &win, long &lowerIndex, long &upperIndex);
    
    /*! \brief Handler of event going one directiry up
    * \param win Current window
    */
    static void Event_GoBack(Window &win);

    /*! \brief Handler of event going in selected directory
    * \param win Current window
    */
    static void Event_GoIntoSelectedItem(Window &win);
    
    /*! \brief Handler of copying of selected element
    * \param win Current window
    */
    static void Event_Copy(Window &win);
    
    /*! \brief Handler of pasting into selected fodler
    * \param win Current window
    */
    static void Event_Paste(Window &win);
    
    /*! \brief Handler of file creating
    * \param win Current window
    */
    static void Create_File(Window &win);
    
    /*! \brief Handler of folder creating
    * \param win Current window
    */
    static void Create_Folder(Window &win);
    
    /*! \brief Handler of symbolic link creating
    * \param win Current window
    */
    static void Create_SymbLink(Window& win);
    
    /*! \brief Handler of deleting of a selected element
    * \param win Current window
    */
    static void Event_Delete(Window &win);

    /*! \brief Handler of searching by regular expression
    * \param win Current window
    */
    static void Event_RegEx(Window &win);

    /*! \brief Handler of adding file in multiple selection mode
    * \param win Current window
    */
    static void Event_AddToMultiple(Window &win);

    /*! \brief Handler of deleting of several selected files
    * \param win Current window
    */
    static void Event_MultipleDelete(Window &win);

    /*! \brief Handler of ddisplaying an error dialog
    * \param win Current window
    * \param message Message to be displayed
    */
    static void Event_ShowErrorDialog(Window &win, const std::string &message);
    
    /*! \brief Handler of multiple copying
    * \param win Current window
    */
    static void Event_MultipleCopy(Window &win);
};


#endif //FILEMANAGER_LOGIC_HPP
