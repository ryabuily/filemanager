#include "../incl/File.hpp"
#include "../incl/FileAbstract.hpp"
#include "../incl/Folder.hpp"
#include <iostream>
#include <fcntl.h>
#include <unistd.h>
#include <fstream>
#include <libgen.h>
#include "../incl/FileSystemException.hpp"

using namespace std;

int File::Create(){
    int fd = creat(absolutePath.data(), permission);
    if(fd != -1){
        close(fd);
        throw FileSystemException(strerror(errno));
    }
    return fd;
}

int File::Delete(){
    int res = remove(absolutePath.data());
    if(res == -1){
        throw FileSystemException(strerror(errno));
    }
    return res;
}

void File::Copy( buffer::CopyBuffer *buffer, bool isClear){
    if(isClear)
        buffer->Erase();
    buffer->GetQueue().push(std::shared_ptr<FileAbstract>(new File(*this)));
}

void File::Paste(const std::string &destination, buffer::CopyBuffer *buffer){
    Folder dir(destination);
    //dir.Create();
    std::ifstream is(buffer->GetOrigin(), std::ios_base::binary);
    std::ofstream os(destination+"/"+basename(const_cast<char*>(this->absolutePath.data())), std::ios_base::binary);
    os << is.rdbuf();
    is.close();
    os.close();
}