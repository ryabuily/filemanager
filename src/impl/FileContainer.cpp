#include "../incl/FileContainer.hpp"
#include "../incl/Folder.hpp"

bool FileContainer::Navigate(std::vector<std::shared_ptr<FileAbstract> >& targetContent){
    targetContent.clear();
    auto data = Scan();
    targetContent.insert(targetContent.end(),data.begin(),data.end());
    return true;

}