#include "../incl/Window.hpp"
#include "../incl/Constants.hpp"
#include <ncurses.h>
#include <libgen.h>
#include <iostream>
#include <regex>



void Window::Erase() {
    erase();
    refresh();
}

void Window::Init() {
    initscr();
    start_color();
    noecho();
    init_pair(SELECTED_PAIR,COLOR_WHITE, COLOR_BLUE);
    init_pair(TYPE_FILE, COLOR_CYAN, COLOR_BLACK);
    init_pair(TYPE_LINK, COLOR_MAGENTA, COLOR_BLACK);
    init_pair(TYPE_FOLDER, COLOR_WHITE, COLOR_BLACK);
    init_pair(SELECTED_MULTIPLE, COLOR_BLACK, COLOR_YELLOW);
    keypad(stdscr, true);
}

void Window::Close() {
    endwin();
}

void Window::Draw(const long lowerIndex, const size_t upperIndex) {
    bool isSelected = false;
    for(size_t i = std::max(lowerIndex, 0l); i< std::min(upperIndex, filesList.size()); ++i){
        switch (filesList[i]->Dir()){
            case FileAbstract::LINK:
                attron(COLOR_PAIR(TYPE_LINK));
                break;
            case FileAbstract::FILE:
                attron(COLOR_PAIR(TYPE_FILE));
                break;
            case FileAbstract::DIRECTORY:
                attron(COLOR_PAIR(TYPE_FOLDER));
            break;

            default:
                break;
        }

        if(selectedIndex == (long)(i)) {
            attron(COLOR_PAIR(SELECTED_PAIR));
            isSelected = true;
        }
        if(markedFiles[i]){
            attron(COLOR_PAIR(SELECTED_MULTIPLE));
        }
        
        printw(filesList[i]->Name().data());
        printw("\n");
        attroff(COLOR_PAIR(TYPE_FILE));
        if(isSelected){
            attroff(COLOR_PAIR(SELECTED_PAIR));
        }
    }
    refresh();
}

void Window::OpenFolder() {
    Draw();
}

void Window::Copy() {
    if(filesList.size()){
        filesList[selectedIndex].get()->Copy(&buffer, true);
    }
}


void Window::Paste() {
    std::string s = filesList[0].get()->Path();
    filesList[selectedIndex].get()->Paste(FileAbstract::DirName(s.data()), &buffer);
}

std::vector<std::shared_ptr<FileAbstract> > Window::Search(const std::string &expr) {
    std::vector<std::shared_ptr<FileAbstract> > res;
    std::regex file_regex(expr);
    for(auto i: filesList){
        std::string path = i.get()->Path();
        char* n = const_cast<char*> (path.data());
        std::string name = basename(n);
        if(std::regex_search(name, file_regex)){
            res.push_back(i);
        }
    }
    return res;
}


WINDOW *create_newwin(int height, int width, int starty, int startx){
    WINDOW *local_win;

	local_win = newwin(height, width, starty, startx);
	box(local_win, 0 , 0);		/* 0, 0 gives default characters 
					 * for the vertical and horizontal
					 * lines			*/
	wrefresh(local_win);		/* Show that box 		*/

	return local_win;
}

void destroy_win(WINDOW *local_win){	
	wborder(local_win, ' ', ' ', ' ',' ',' ',' ',' ',' ');
	wrefresh(local_win);
	delwin(local_win);
}

void Window::DrawNameDialog( WINDOW *win, const std::string &helloText, const std::string &curName){
    werase(win);
    wborder(win, '|', '|', '-', '-', '+', '+', '+', '+');
    wmove(win, 1, 1);
    wprintw(win, helloText.data());
    wmove(win, 2,1);
    wprintw(win, curName.data());
    //wmove(win, 3, 1);
    wrefresh(win);
}

bool isAdequateSymbol(char c){
    return ( (c != 47)&&(c>=32));
}
std::string Window::GetName(const std::string &fileType){
    WINDOW *my_win;
    int startx, starty, width, height;
    std::string fileName;
    int ch;
    std::string helloText = "     Type the name of "+fileType + " and press ENTER";
	
    height = 5;
	width = 50;
	starty = (LINES - height) / 2;	/* Calculating for a center placement */
	startx = (COLS - width) / 2;

    my_win = create_newwin(height, width, starty, startx);
    wmove(my_win, 0, 0);
    
    DrawNameDialog(my_win, helloText, fileName);

    while((ch = getch()) != -1){
        switch (ch)
        {
        case 10:
        case KEY_ENTER:
            destroy_win(my_win);
            return fileName;
        case 127:
        case KEY_DC:
            fileName.pop_back();
            DrawNameDialog(my_win, helloText, fileName);
        break;
        default:
            if(isAdequateSymbol(ch)){
                fileName+=(char)ch;
                DrawNameDialog(my_win, helloText, fileName);
            }
            break;
        }
    }
    return fileName;
}

std::string Window::GetRegEx(){
    WINDOW *my_win;
    int startx, starty, width, height;
    std::string fileName;
    int ch;
    std::string helloText = "     Type the regular expression and press ENTER";
	
    height = 5;
	width = 50;
	starty = (LINES - height) / 2;	/* Calculating for a center placement */
	startx = (COLS - width) / 2;

    my_win = create_newwin(height, width, starty, startx);
    wmove(my_win, 0, 0);
    
    DrawNameDialog(my_win, helloText, fileName);

    while((ch = getch()) != -1){
        switch (ch)
        {
        case 10:
        case KEY_ENTER:
            destroy_win(my_win);
            return fileName;
        case 127:
        case KEY_DC:
            fileName.pop_back();
            DrawNameDialog(my_win, helloText, fileName);
        break;
        default:
            if(isAdequateSymbol(ch)){
                fileName+=(char)ch;
                DrawNameDialog(my_win, helloText, fileName);
            }
            break;
        }
    }
    return fileName;
}

void Window::ShowErrorDiaglog(const std::string message){

     WINDOW *my_win;
    int startx, starty, width, height;
    std::string fileName;
    int ch;
	
    height = 5;
	width = 50;
	starty = (LINES - height) / 2;	/* Calculating for a center placement */
	startx = (COLS - width) / 2;

    my_win = create_newwin(height, width, starty, startx);
    wmove(my_win, 0, 0);
    
    DrawNameDialog(my_win, message, fileName);

    while((ch = getch()) != -1){
        if(ch == 10 || ch == KEY_ENTER)
            destroy_win(my_win);
    }      
 }