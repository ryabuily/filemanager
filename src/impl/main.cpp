#include <iostream>
#include <ncurses.h>
#include "../incl/Window.hpp"
#include "../incl/Constants.hpp"
#include "../incl/Logic.hpp"
//#include <signal.h>


int main() {
    Window win;

    long lowerIndex=0, upperIndex = lowerIndex+MAX_LINES-1;

    bool multipleSelectionMode = false;
    win.Draw();
    int ch;
    while( (ch = getch()) != -1) {
        switch (ch) {
            default:
                if(!multipleSelectionMode){
                    win.ResetSelected(lowerIndex, upperIndex);
                }
                break;
            case KEY_DOWN:
                if(!multipleSelectionMode){
                    win.ResetSelected(lowerIndex, upperIndex);
                }
                Logic::ButtonPressed_Down(win, lowerIndex, upperIndex);
                break;
            case KEY_UP:
                if(!multipleSelectionMode){
                    win.ResetSelected(lowerIndex, upperIndex);
                }
                Logic::ButtonPressed_Up(win, lowerIndex, upperIndex);
                break;
            case KEY_RIGHT:
                multipleSelectionMode = false;
                win.ResetSelected(lowerIndex, upperIndex);
                Logic::Event_GoIntoSelectedItem(win);
                lowerIndex=0;
                upperIndex = lowerIndex+MAX_LINES-1;
                refresh();
                break;
            case KEY_LEFT:
                multipleSelectionMode = false;
                win.ResetSelected(lowerIndex, upperIndex);
                Logic::Event_GoBack(win);
                lowerIndex=0;
                upperIndex = lowerIndex+MAX_LINES-1;
                refresh();
                break;
            case KEY_F(1):
                if(multipleSelectionMode){

                }
                else{
                    Logic::Event_Copy(win);
                }
                break;
            case KEY_F(2):
                Logic::Event_Paste(win);
                break;
            case KEY_F(3):
                multipleSelectionMode = false;
                if(!multipleSelectionMode){
                    win.ResetSelected(lowerIndex, upperIndex);
                }
                win.ResetSelected(lowerIndex, upperIndex);
                Logic::Create_File(win);
                break;
            case KEY_F(4):
                multipleSelectionMode = false;
                if(!multipleSelectionMode){
                    win.ResetSelected(lowerIndex, upperIndex);
                }
                win.ResetSelected(lowerIndex, upperIndex);
                Logic::Create_Folder(win);
                break;
            case KEY_F(5):
                multipleSelectionMode = false;
                if(!multipleSelectionMode){
                    win.ResetSelected(lowerIndex, upperIndex);
                }
                win.ResetSelected(lowerIndex, upperIndex);
                Logic::Create_SymbLink(win);
                break;
            case KEY_F(6):
                multipleSelectionMode = false;
                if(!multipleSelectionMode){
                    win.ResetSelected(lowerIndex, upperIndex);
                }
                win.ResetSelected(lowerIndex, upperIndex);
                Logic::Event_RegEx(win);
            break;

            case KEY_F(7):
                multipleSelectionMode = !multipleSelectionMode;
            break;

            case 127:
            case KEY_DC:
                if(multipleSelectionMode){
                    Logic::Event_MultipleDelete(win);
                    multipleSelectionMode = false;
                    win.ResetSelected(lowerIndex, upperIndex);
                }
                else{
                    Logic::Event_Delete(win);
                }
                break;
            case 10:
            case KEY_ENTER:
                if(multipleSelectionMode){
                    Logic::Event_AddToMultiple(win);
                    win.MarkSelected(lowerIndex, upperIndex);
                }
                else{
                    win.Close();
                    return 0;
                }
               
                break;
        }
    }

    return 0;
}