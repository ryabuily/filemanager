#include "../incl/FileAbstract.hpp"
#include <sys/stat.h>
#include <libgen.h>

int FileAbstract::isDirectory (const char* path){
    struct stat statbuf;
        if (stat(path, &statbuf) != 0)
            return 0;
        return S_ISDIR(statbuf.st_mode); 
}

std::string FileAbstract::DirName(const std::string &current){
    std::string res;
    auto elem = current.rfind("/");
    if(elem>0){
        return current.substr(0, elem);
    }
    else return "";
}

int FileAbstract::Rename(const std::string &newName){
    std::string dirName = dirname( const_cast<char*>(absolutePath.data()) );
    std::string newPath = (dirName+"/"+newName);
    int res = ::rename(absolutePath.data(), newPath.data());
    absolutePath = newPath;
    return res;
}

bool FileAbstract::Navigate(std::vector<std::shared_ptr<FileAbstract> >& targetContent){
    return false;
}

bool FileAbstract::isDir(){
    return dir==FileType::DIRECTORY;
}