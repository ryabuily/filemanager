#include "../incl/Folder.hpp"
#include "../incl/File.hpp"
#include "../incl/Link.hpp"
#include "../incl/FileSystemException.hpp"
#include <errno.h>
#include <sys/stat.h>
#include <dirent.h>
#include <libgen.h>
#include <unistd.h>
#include <fstream>

int Folder::Create(){
    int res = mkdir(Folder::absolutePath.data(), Folder::permission);
    if(res == -1){
        throw FileSystemException(strerror(errno));
    }
    return res;
}

int Folder::Delete(){
    auto content = Scan();
    for(auto i: content){
        i->Delete();
    }
    int res = rmdir(Folder::absolutePath.data());
    if(res == -1){
        throw FileSystemException(strerror(errno));
    }
    return res;
}

std::vector<std::shared_ptr<FileAbstract> > Folder::Scan(){
    std::vector<std::shared_ptr<FileAbstract> > Result;

    //inspired by https://stackoverflow.com/questions/18402428/how-to-properly-use-scandir-in-c
    struct dirent **namelist;
    int n;
    n = scandir(absolutePath.data(), &namelist, NULL, alphasort);
    if (n < 0){
        throw FileSystemException(strerror(errno));
    }
    else {
        while (n--) {
            std::string dirName = namelist[n]->d_name;
            if(dirName != "." && dirName != ".."){
                int fileType = namelist[n]->d_type;
                std::shared_ptr<FileAbstract> elem;
                switch (fileType)
                {
                case DT_DIR:
                    elem = std::shared_ptr<FileAbstract>(new Folder(absolutePath+(absolutePath!="/"? "/" : "") +dirName));
                    elem.get()->SetDir(FileAbstract::FileType::DIRECTORY);
                    Result.push_back(elem);
                    break;
                case DT_LNK:
                    elem = std::shared_ptr<FileAbstract>(new Link(absolutePath+(absolutePath!="/"? "/" : "") +dirName));
                    elem.get()->SetDir(FileAbstract::FileType::LINK);
                    Result.push_back(elem);
                    break;
                case DT_REG:
                    elem = std::shared_ptr<FileAbstract>(new File((absolutePath+(absolutePath!="/"? "/" : "") +dirName)));
                    elem.get()->SetDir(FileAbstract::FileType::FILE);
                    Result.push_back(elem);
                    break;
                default:
                    break;
                }
            }
            free(namelist[n]);
        }
        free(namelist);
    }
    return Result;
}
std::vector<std::shared_ptr<FileAbstract> > Folder::CollectAll(){
    std::vector<std::shared_ptr<FileAbstract> > res;
    auto content = Scan();
    for (auto i: content) {
        res.push_back(i);
        if(i->isDir()){
            std::vector<std::shared_ptr<FileAbstract> > tmp = dynamic_cast<FileContainer*>(i.get())->CollectAll();
            res.insert(res.end(), tmp.begin(), tmp.end());
        }
    }
    return res;
}

void Folder::Copy(buffer::CopyBuffer *buffer, bool isClean){
    if(isClean)
        buffer->Erase();
    auto res = CollectAll();

    buffer->Push(std::shared_ptr<FileAbstract>(new Folder(*this)));
    for(auto i= res.begin(); i != res.cend(); ++i){
        buffer->Push(*i);
    }

    buffer->setOrigin(FileAbstract::DirName(absolutePath));
}

void Folder::Paste(const std::string &destination, buffer::CopyBuffer *buffer){
    std::string pastePath = destination+"/"+basename(const_cast<char*>(absolutePath.data()));
   while(buffer->GetQueue().size()>0){
        auto i = buffer->GetQueue().front();
        buffer->GetQueue().pop();
        std::string path = i->Path();
        std::string tail;
        if(!strncmp(i->Path().data(), buffer->GetOrigin().data(), buffer->GetOrigin().size())){
            tail = i->Path().substr(buffer->GetOrigin().size()+1);
        }
        std::string newAbsPath = pastePath+"/"+tail;
        if(i->isDir()){
            Folder tmpFolder(newAbsPath);
            tmpFolder.Create();
        }
        else{
            File tmpFile(newAbsPath);
            tmpFile.Create();
            std::ifstream is(path, std::ios_base::binary);
            std::ofstream os(newAbsPath, std::ios_base::binary);
            os << is.rdbuf();
        }
    }
}