#include "../incl/Link.hpp"
#include "../incl/FileSystemException.hpp"
#include <unistd.h>

Link::Link(const std::string &n): File(n), name(n), target(nullptr){
    dir = FileAbstract::FileType::LINK;
    struct stat info;
    memset(&info,0,sizeof(info));
    int status = lstat(n.data(), &info);
    if(status == -1){
        throw FileSystemException(strerror(errno));
    }

    char *content = new char[info.st_size];
    memset(content,0,info.st_size);
    int res = readlink(n.data(), content, info.st_size);
    if(res == -1){
        throw FileSystemException(strerror(errno));
    }

    ref = content;

    if(FileAbstract::isDirectory(content))
        target.reset(new Folder(content));
    else target.reset(new File(content));  

    delete[] content;  
}

int Link::Create(){
    int res = symlink(ref.data(), name.data());
    if(res == -1){
        throw FileSystemException(strerror(errno));
    }
    return res;
}