#include "../incl/Logic.hpp"
#include "../incl/Link.hpp"
#include "../incl/File.hpp"
#include "../incl/Folder.hpp"
#include "../incl/Window.hpp"
#include "../incl/FileAbstract.hpp"
#include "../incl/FileSystemException.hpp"
#include <errno.h>
#include <libgen.h>


void Logic::ButtonPressed_Up(Window &win, long &lowerIndex, long &upperIndex) {
    win.DecrementIndex();
    if (win.Index() < 0) {
        win.SetIndex(win.List().size() - 1);
        if(win.List().size()>0){
            upperIndex = win.List().size() -1;
            lowerIndex = std::max(0l, (long)(upperIndex - MAX_LINES-1l));
        }else{
            upperIndex=0;
            lowerIndex=0;
        }
    }
    
    if (win.Index() < lowerIndex) {
        lowerIndex--;
        upperIndex--;
    }
    win.Erase();
    win.Draw(lowerIndex, upperIndex);
}
void Logic::ButtonPressed_Down(Window &win, long &lowerIndex, long &upperIndex) {
    win.IncrementIndex();
    if ( (long)(win.List().size() ) == win.Index()) {
        win.SetIndex(0);
        lowerIndex = 0;
        upperIndex = std::min( lowerIndex + MAX_LINES-1,(long)win.List().size());
    }
    
    if (win.Index() >= upperIndex) {
        upperIndex++;
        lowerIndex++;
    }
    win.Erase();
    win.Draw(lowerIndex, upperIndex);
}


void Logic::Event_Delete(Window &win) {
    auto elem = win.List()[win.Index()];
    try{
        elem.get()->Delete();
    }
    catch(FileSystemException &ex){
        Event_ShowErrorDialog(win, ex.what());
    }
    win.DeleteFromVector(elem);
    win.Erase();
    win.Draw();
}
void Logic::Create_SymbLink(Window &win) {
    auto elem = win.List()[win.Index()].get();
    std::string ref = elem->Path();
    std::string name = win.GetName("link");
    std::string prefix = FileAbstract::DirName(ref)+"/";
    Link link(ref, prefix+name);
    try{
        link.Create();
        win.AddLink(link);
        win.Erase();
        win.Draw();
    }
    catch(FileSystemException &ex){
        std::string msg = ex.what();
    }
}
void Logic::Create_File(Window &win) {
    
    std::string name = win.GetName("file");
    try{
        std::string fullPath = FileAbstract::DirName( win.List()[win.Index()]->Path())+"/"+ name;
        File file(fullPath);
        file.Create();
        win.AddFile(file);
        win.Erase();
        win.Draw();
    }
    catch(FileSystemException &ex){
        Event_ShowErrorDialog(win, ex.what());
    }
}
void Logic::Create_Folder(Window &win) {
    std::string name = win.GetName("folder");
    try{
        std::string fullPath = FileAbstract::DirName( win.List()[win.Index()]->Path())+"/"+ name;
        Folder folder(fullPath);
        folder.Create();
        win.AddFolder(folder);
        win.Erase();
        win.Draw();
    }
    catch(FileSystemException &ex){
        Event_ShowErrorDialog(win, ex.what());
    }
}
void Logic::Event_Copy(Window &win) {
    win.Copy();
}
void Logic::Event_Paste(Window &win) {
    win.Paste();
}
void Logic::Event_GoIntoSelectedItem(Window &win) {
    auto ptrToKeep =win.List()[win.Index()];
    try{
        if(!ptrToKeep->Navigate(win.List())){
            Event_ShowErrorDialog(win, "Can't navigate :(");
        }
    }
    catch(FileSystemException &ex){
         Event_ShowErrorDialog(win, ex.what()
         );
    }
    win.SetIndex(0);
    win.Erase();
    win.Draw();
}

void Logic::Event_GoBack(Window &win) {
    auto elem = win.List()[win.Index()].get();
    auto dupdata = FileAbstract::DirName (elem->Path());
    dupdata = FileAbstract::DirName(dupdata);
    Folder dir(dupdata);
    if(!dir.Navigate(win.List())){
        Event_ShowErrorDialog(win, "Can't navigate :(");
    }else{
        win.SetIndex(0);
        win.Erase();
        win.Draw();
    }
}

void Logic::Event_RegEx(Window &win){
    std::string regex = win.GetRegEx();

    auto res = win.Search(regex);
    win.SetList(res);
    win.Erase();
    win.Draw(); 
}

void Logic::Event_AddToMultiple(Window &win){
    win.MultipleMode_Add();

}

void Logic::Event_MultipleDelete(Window &win){    
    try{
        for(auto i: win.Files()){
            i.get()->Delete();
        }
    }
    catch(FileSystemException &ex){
        Event_ShowErrorDialog(win, ex.what());
    }
}

void Logic::Event_MultipleCopy(Window &win){
     try{
        for(auto i: win.Files()){
            i.get()->Copy(win.Buffer(), true);
        }
    }
    catch(FileSystemException &ex){
        Event_ShowErrorDialog(win, ex.what());
    }
}

void Logic::Event_ShowErrorDialog(Window &win, const std::string &message){
    win.ShowErrorDiaglog(message);
}