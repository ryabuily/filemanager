CC=g++
CXXFLAGS=-Wall -pedantic -Wextra -g -std=c++11
OBJS= ryabuily/File.o ryabuily/Folder.o ryabuily/Logic.o ryabuily/Window.o ryabuily/FileAbstract.o ryabuily/Link.o ryabuily/FileContainer.o
LIBS=-lcurses -lncurses
all: clean compile run
clean:
	if [ -d ryabuily ]; then rm -f ryabuily/*; else mkdir ryabuily; fi
compile : $(OBJS) src/impl/main.cpp
	$(CC) src/impl/main.cpp $(OBJS) $(CXXFLAGS) $(LIBS)
$(OBJS): ryabuily/%.o: src/impl/%.cpp src/incl/%.hpp
	$(CC) -c src/impl/$(addsuffix .cpp,$(notdir $(basename $@))) -o $@ $(LIBS) $(CXXFLAGS)
run:    
	./a.out
doc:  
	@doxygen doxyg
