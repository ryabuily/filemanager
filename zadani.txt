File Manager

The program for managing files / folders (source of inspiration Midnight Commander).

Implement the following functionality:
1. Creating and deleting files / folders / symbolic links ...
2. Copying and moving files / folders / symbolic links ...
3. The ability to perform operations on a bulk of files specified by a regular expression
Navigation between folders
4. User Interface: Display directories on which operations are being performed and a visual distinction of files / folders / symbolic links ...

Voluntary possible extension:
1. Two panels interface
2. GUI

Using Polymorphism (recommended):
1. File operations
2. File Types
3. UI
